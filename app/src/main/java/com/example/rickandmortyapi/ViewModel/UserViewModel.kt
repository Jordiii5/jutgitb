package com.example.rickandmortyapi.ViewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.rickandmortyapi.Api.Data
import com.example.rickandmortyapi.Api.Repository
import com.example.rickandmortyapi.Api.Result
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class UserViewModel: ViewModel(){
    private val repository = Repository()
    var data = MutableLiveData<Data?>()
    lateinit var currentUser: com.example.rickandmortyapi.Api.Result
    var currentPage = 1
    var name = ""

    init {
        fetchData()
    }
    fun fetchData(){
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getCharacters("character?page=$currentPage")
            withContext(Dispatchers.Main) {
                if(response.isSuccessful){
                    data.postValue(response.body())
                }
                else{
                    Log.e("Error :", response.message())
                }
            }
        }
    }
    fun fetchSearch(){
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getCharacters("character/?name=$name")
            withContext(Dispatchers.Main){
                if(response.isSuccessful){
                    data.postValue(response.body())
                }
                else{
                    Log.e("Error :", response.message())
                }
            }
        }
    }
    fun setSelectedUser(user: com.example.rickandmortyapi.Api.Result){
        currentUser = user
    }



    fun characterData(id:Int): Result?{
        var character:com.example.rickandmortyapi.Api.Result? = null
        var i = 0

        while (i in data.value!!.results.indices&&character==null){
            if(data.value!!.results[i].id == id) character = data.value!!.results[i]
            i++
        }

        return character
    }
}
