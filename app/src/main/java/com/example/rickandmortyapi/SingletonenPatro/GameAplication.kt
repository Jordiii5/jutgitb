package com.example.rickandmortyapi.SingletonenPatro

import android.app.Application
import androidx.room.Room
import com.example.rickandmortyapi.RoomDatabase.ContactDatabase

class ContactApplication: Application() {
    companion object {
        lateinit var database: ContactDatabase
    }
    override fun onCreate() {
        super.onCreate()
        database = Room.databaseBuilder(this,
            ContactDatabase::class.java,
            "ContactDatabase2").build()
    }
}
