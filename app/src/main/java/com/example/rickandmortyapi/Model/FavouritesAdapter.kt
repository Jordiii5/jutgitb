package com.example.rickandmortyapi.Model

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.rickandmortyapi.R
import com.example.rickandmortyapi.RoomDatabase.ContactEntity
import com.example.rickandmortyapi.View.FavOnClickListener
import com.example.rickandmortyapi.View.FavouritesFragment
import com.example.rickandmortyapi.databinding.ItemFavouriteUserBinding

class FavouritesAdapter(private val characters: MutableList<ContactEntity>, private val listener: FavOnClickListener): RecyclerView.Adapter<FavouritesAdapter.ViewHolder>() {
    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ItemFavouriteUserBinding.bind(view)
        fun setListener(character: ContactEntity){
            binding.root.setOnClickListener {
                listener.onClick(character)
            }
        }
    }

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavouritesAdapter.ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_favourite_user, parent, false)
        return ViewHolder(view)
    }
    override fun getItemCount(): Int {
        return characters.size
    }
    override fun onBindViewHolder(holder: FavouritesAdapter.ViewHolder, position: Int) {
        val character = characters[position]
        with(holder){
            setListener(character)
            binding.characterName.text = character.name
            Glide.with(context)
                .load(character.image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(binding.characterImg)
        }
    }


}