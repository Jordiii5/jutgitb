package com.example.rickandmortyapi.Model

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.rickandmortyapi.Api.Result
import com.example.rickandmortyapi.View.OnClickListener
import com.example.rickandmortyapi.R
import com.example.rickandmortyapi.databinding.ItemUserBinding

class UserAdapter (private val characters: List<Result>, private val listener: OnClickListener): RecyclerView.Adapter<UserAdapter.ViewHolder>() {
    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ItemUserBinding.bind(view)
        fun setListener(character: Result){
            binding.root.setOnClickListener {
                listener.onClick(character)
            }
        }
    }

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_user, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return characters.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val character = characters[position]
        with(holder){
            setListener(character)
            binding.characterName.text = character.name
            if(character.gender == "Male") binding.characterGender.setImageResource(R.drawable.male)
            else binding.characterGender.setImageResource(R.drawable.female)
            Glide.with(context)
                .load(character.image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(binding.characterImg)
        }
    }
}