package com.example.rickandmortyapi.RoomDatabase

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.rickandmortyapi.Dao.ContactDao


@Database(entities = arrayOf(ContactEntity::class), version = 1)
abstract class ContactDatabase: RoomDatabase() {
    abstract fun contactDao(): ContactDao
}
