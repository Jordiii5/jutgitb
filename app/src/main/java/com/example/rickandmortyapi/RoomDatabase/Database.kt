package com.example.rickandmortyapi.RoomDatabase

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.rickandmortyapi.Api.Location
import com.example.rickandmortyapi.Api.Origin

@Entity(tableName = "ContactEntity")
data class ContactEntity(
    @PrimaryKey(autoGenerate = true)  var id: Int = 0,
    var name: String,
    var image: String
)