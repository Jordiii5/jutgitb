package com.example.rickandmortyapi.Api

data class Origin(
    val name: String,
    val url: String
)