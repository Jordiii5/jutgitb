package com.example.rickandmortyapi.Api

data class Data(
    val info: Info,
    val results: List<Result>
)