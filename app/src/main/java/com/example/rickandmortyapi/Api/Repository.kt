package com.example.rickandmortyapi.Api

import com.example.rickandmortyapi.View.ApiInterface

class Repository {
    val apiInterface = ApiInterface.create()
    suspend fun getCharacters(url: String) = apiInterface.getCharacters(url)
}