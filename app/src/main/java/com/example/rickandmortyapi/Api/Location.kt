package com.example.rickandmortyapi.Api

data class Location(
    val name: String,
    val url: String
)