package com.example.rickandmortyapi.Dao

import androidx.room.Dao
import androidx.room.*
import com.example.rickandmortyapi.RoomDatabase.ContactEntity

@Dao
interface ContactDao {
    @Query("SELECT * FROM ContactEntity")
    fun getAllContacts(): MutableList<ContactEntity>
    //@Query("SELECT * FROM ContactEntity where name = :contactName")
    //fun getContactsByName(contactName: String): MutableList<ContactEntity>
    @Insert
    fun addContact(contactEntity: ContactEntity)
    @Update
    fun updateContact(contactEntity: ContactEntity)
    @Delete
    fun deleteContact(contactEntity: ContactEntity)
}
