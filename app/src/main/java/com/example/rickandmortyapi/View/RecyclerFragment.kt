package com.example.rickandmortyapi.View

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.rickandmortyapi.Api.Result
import com.example.rickandmortyapi.Model.UserAdapter
import com.example.rickandmortyapi.R
import com.example.rickandmortyapi.ViewModel.UserViewModel
import com.example.rickandmortyapi.databinding.FragmentRecyclerBinding

class RecyclerFragment : Fragment(), OnClickListener {

    private lateinit var binding: FragmentRecyclerBinding
    private lateinit var characterAdapter: UserAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    private val viewModel: UserViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreate(savedInstanceState)
        binding = FragmentRecyclerBinding.inflate(layoutInflater)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.data.observe(viewLifecycleOwner){
            setUpRecyclerView(it!!.results)
        }
        if (viewModel.currentPage == 1){
            binding.botonatras.visibility = View.INVISIBLE
        }
        binding.botonnext.setOnClickListener {
            binding.botonatras.visibility = View.VISIBLE
            viewModel.currentPage += 1
            viewModel.fetchData()
        }
        binding.botonatras.setOnClickListener {
            if (viewModel.currentPage > 1){
                viewModel.currentPage -= 1
                viewModel.fetchData()
                if (viewModel.currentPage == 1){
                    binding.botonatras.visibility = View.INVISIBLE
                }
            }
        }
        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query != null) {
                    if (query != "") {
                        viewModel.name = query
                        viewModel.fetchSearch()
                        binding.botonatras.visibility = View.INVISIBLE
                        binding.botonnext.visibility = View.INVISIBLE
                    }
                    else {
                        viewModel.fetchData()
                        if (viewModel.currentPage == 1){
                            binding.botonnext.visibility= View.VISIBLE
                        }
                        else {
                            binding.botonatras.visibility = View.VISIBLE
                            binding.botonnext.visibility= View.VISIBLE
                        }
                    }
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText != null) {
                    if (newText != "") {
                        viewModel.name = newText
                        viewModel.fetchSearch()
                        binding.botonatras.visibility = View.INVISIBLE
                        binding.botonnext.visibility = View.INVISIBLE
                    }
                    else {
                        viewModel.fetchData()
                        if (viewModel.currentPage == 1){
                            binding.botonnext.visibility= View.VISIBLE
                        }
                        else {
                            binding.botonatras.visibility = View.VISIBLE
                            binding.botonnext.visibility= View.VISIBLE
                        }
                    }
                }
                return false
            }
        })
    }
    private fun setUpRecyclerView(listOfUsers: List<Result>){
        characterAdapter = UserAdapter(listOfUsers , this)
        linearLayoutManager = LinearLayoutManager(context)
        binding.recyclerView.apply {
            setHasFixedSize(true) //Optimitza el rendiment de l’app
            layoutManager = linearLayoutManager
            adapter = characterAdapter
        }
    }

    override fun onClick(character: Result) {
        viewModel.setSelectedUser(character)
        val action = RecyclerFragmentDirections.actionRecyclerFragmentToCharacterFragment(viewModel.currentUser.id,viewModel.currentUser.favourite)
        findNavController().navigate(action)
    }
}