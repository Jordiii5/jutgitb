package com.example.rickandmortyapi.View

import com.example.rickandmortyapi.Api.Data
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Url

interface ApiInterface {
    @GET()
    suspend fun getCharacters(@Url url:String): Response<Data>
    companion object {
        val BASE_URL = "https://rickandmortyapi.com/api/"
        fun create(): ApiInterface {
            val client = OkHttpClient.Builder().build()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
            return retrofit.create(ApiInterface::class.java)
        }
    }

    /*
    @GET("character")
    suspend fun getCharacters(): Response<Data>

    @GET("{id}")
    suspend fun getCharacter(@Path("id") characterId: Int): Response<Data>
     */
}