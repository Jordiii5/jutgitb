package com.example.rickandmortyapi.View

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.rickandmortyapi.Model.FavouritesAdapter
import androidx.navigation.fragment.findNavController
import com.example.rickandmortyapi.RoomDatabase.ContactEntity
import com.example.rickandmortyapi.SingletonenPatro.ContactApplication
import com.example.rickandmortyapi.databinding.FragmentFavouritesBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class FavouritesFragment : Fragment(), FavOnClickListener {
    lateinit var binding: FragmentFavouritesBinding
    private lateinit var favoritesAdapter: FavouritesAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager

    private fun getCharacters(): MutableList<ContactEntity>{
        val characters = mutableListOf<ContactEntity>()
        return characters
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentFavouritesBinding.inflate(inflater,container,false)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        favoritesAdapter = FavouritesAdapter(getCharacters(),this)
        linearLayoutManager = LinearLayoutManager(context)

        CoroutineScope(Dispatchers.IO).launch {
            val characters = ContactApplication.database.contactDao().getAllContacts()
            withContext(Dispatchers.Main){
                setUpRecyclerFav(characters)
            }
        }
    }

    fun setUpRecyclerFav(resultList:MutableList<ContactEntity>){
        val myAdapter = FavouritesAdapter(resultList,this)
        binding.recyclerViewFav.apply {
            setHasFixedSize(true) //Optimitza el rendiment de l’app
            layoutManager = linearLayoutManager
            adapter = myAdapter
        }
    }

    override fun onClick(character: ContactEntity) {
        val action = FavouritesFragmentDirections.actionFavouritesFragmentToCharacterFragment(character.id,true)
        findNavController().navigate(action)
    }


}