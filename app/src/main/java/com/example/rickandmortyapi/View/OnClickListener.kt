package com.example.rickandmortyapi.View

import com.example.rickandmortyapi.Api.Result

interface OnClickListener {
    fun onClick(character: Result)
}