package com.example.rickandmortyapi.View

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.rickandmortyapi.Model.UserAdapter
import com.example.rickandmortyapi.R
import com.example.rickandmortyapi.RoomDatabase.ContactEntity
import com.example.rickandmortyapi.SingletonenPatro.ContactApplication
import com.example.rickandmortyapi.ViewModel.UserViewModel
import com.example.rickandmortyapi.databinding.FragmentCharacterBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CharacterFragment : Fragment() {

    lateinit var binding: FragmentCharacterBinding
    private val viewModel: UserViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCharacterBinding.inflate(layoutInflater)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val viewModel = ViewModelProvider(requireActivity())[UserViewModel::class.java]
        val favorito = arguments?.getBoolean("favorite")
        val id = arguments?.getInt("id")
        val character = viewModel.characterData(id!!)

        Glide.with(requireContext())
            .load(character?.image)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(binding.characterImg)
        binding.characterNameText.text = character?.name
        binding.characterStatus.text = character?.status
        binding.characterSpecies.text = character?.species
        binding.characterOrigin.text = character?.origin?.name
        binding.characterLocation.text = character?.location?.name
        if (character != null) {
            if(viewModel.currentUser.gender == "Male") binding.characterGender.setImageResource(R.drawable.male)
            else binding.characterGender.setImageResource(R.drawable.female)
        }
        if (character != null) {
            context?.let {
                Glide.with(it)
                    .load(character?.image)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(binding.characterImg)
            }
        }

        if ( favorito == true) binding.favorito.alpha = 1F
        else binding.favorito.alpha = 0.2F

        binding.favorito.setOnClickListener {
            val characterDetail = ContactEntity(id = character!!.id, name = character!!.name,image = character!!.image)
            if (binding.favorito.alpha == 0.2F){
                binding.favorito.alpha = 1.0F
                character?.favourite = true
                CoroutineScope(Dispatchers.IO).launch {
                    ContactApplication.database.contactDao().addContact(characterDetail)
                }
            }
            else{
                binding.favorito.alpha = 0.2F
                character?.favourite = false
                CoroutineScope(Dispatchers.IO).launch {
                    ContactApplication.database.contactDao().deleteContact(characterDetail)
                }

            }
        }
    }
}