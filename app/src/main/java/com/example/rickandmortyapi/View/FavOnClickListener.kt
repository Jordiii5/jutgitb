package com.example.rickandmortyapi.View

import com.example.rickandmortyapi.RoomDatabase.ContactEntity

interface FavOnClickListener {
    fun onClick(character: ContactEntity)
}